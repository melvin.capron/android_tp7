package com.example.contentprovidercapronbis;

import android.net.Uri;

public class UserList {
    public Uri uri;
    public String column;

    /**
     * Provides list information
     *
     * @param uri for source
     * @param column to show
     */
    public UserList(Uri uri, String column){
        this.uri = uri;
        this.column = column;
    }

    /**
     *
     * @return uri
     */
    public Uri getUri() {
        return uri;
    }

    /**
     *
     * @return column
     */
    public String getColumn() {
        return column;
    }
}
