package com.example.contentprovidercapronbis;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    final int ACTIVITY_MANAGER = 1;
    static final String MODE = "mode";
    static final String ACTIVITY_RESULT = "ACTIVITY_RESULT";

    static final String DICTIONNARY_ACTIVITY = "DICTIONNARY_ACTIVITY";
    static final String CONTACT_ACTIVITY = "CONTACT_ACTIVITY";

    static final int ADD_WORD_DICTIONNARY_ACTIVITY = 1;
    static final int ADD_CONTACT_ACTIVITY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init dictionnary activity
        Button dictionnary = findViewById(R.id.dictionnary);

        dictionnary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               pushNewIntent(DICTIONNARY_ACTIVITY, addWordDictionnaryCallback(), ADD_WORD_DICTIONNARY_ACTIVITY);
            }
        });

        //Init contact activity
        Button contact = findViewById(R.id.contact);

        contact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pushNewIntent(CONTACT_ACTIVITY, addContactCallback(), ADD_CONTACT_ACTIVITY);
            }
        });
    }

    /**
     * Will push a new activity based on parameters
     *
     * @param mode of the intent
     * @param addCallback callback for add to list
     */
    private void pushNewIntent(String mode, Intent addCallback, int activityCode){
        Intent activityManagerIntent = new Intent(getApplicationContext(), ActivityManager.class);

        activityManagerIntent.putExtra(MODE, mode);
        activityManagerIntent.putExtra(Intent.EXTRA_INTENT, addCallback);
        activityManagerIntent.putExtra(ACTIVITY_RESULT, activityCode);

        startActivityForResult(activityManagerIntent, ACTIVITY_MANAGER);
    }

    /**
     *
     * @return Intent which will redirect to add dictionnary word
     */
    private Intent addWordDictionnaryCallback(){
        //to handle result
        return new Intent(getApplicationContext(), DictionnaryAddActivity.class);
    }

    /**
     *
     * @return Intent which will redirect to add contact
     */
    private Intent addContactCallback(){
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        return intent;
    }
}