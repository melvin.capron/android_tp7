package com.example.contentprovidercapronbis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class DictionnaryAddActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // call the parent to fill edit fields with existing ones, and to tell what activity to show
        setContentView(R.layout.dictionnary_add_word);

        Button addWordButton = findViewById(R.id.add);

        addWordButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String word = ((EditText) findViewById(R.id.newWord)).getText().toString();

                Intent displayText = new Intent(getApplicationContext(), ActivityManager.class);

                //put extra to next intent
                displayText.putExtra("word", word);

                setResult(RESULT_OK, displayText);
                finish();
            }
        });
    }
}