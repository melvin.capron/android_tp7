package com.example.contentprovidercapronbis;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.UserDictionary;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ActivityManager extends AppCompatActivity {
    ArrayAdapter<String> arrayAdapter = null;
    List<String> sourceList = new ArrayList<>();
    Map<String, UserList> sources = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_activity);

        //initializations
        initiateSwitchOrder();
        initiateAddButton();
        initiateRemoveButton();

        //first population of the list view
        populateListView();
    }

    /**
     * Will initiate the remove button
     */
    private void initiateRemoveButton() {
        Button removeButton = findViewById(R.id.delete);

        removeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                removeFromList();
            }
        });

    }

    /**
     * Will initiate the add button
     */
    private void initiateAddButton() {
        Button addButton = findViewById(R.id.add);

        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivityForResult((Intent) getIntent().getParcelableExtra(Intent.EXTRA_INTENT), getIntent().getIntExtra(MainActivity.ACTIVITY_RESULT, 0));
            }
        });
    }

    /**
     * Will initiate the switch order button
     */
    private void initiateSwitchOrder() {
        Switch sortOrder = findViewById(R.id.sortOrder);

        sortOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                populateListView();
            }
        });
    }

    /**
     * @return The source of listview for contacts or dictionnary
     */
    private UserList getSource() {
        if (this.sources.size() == 0) {
            Map<String, UserList> sources = new HashMap<>();

            //Push all data needed to fetch source to for contacts
            UserList contactsData = new UserList(
                    ContactsContract.RawContacts.CONTENT_URI,
                    ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY
            );
            sources.put(MainActivity.CONTACT_ACTIVITY, contactsData);

            //Push all data needed to fetch source to for dictionnary
            UserList dictionnaryData = new UserList(
                    UserDictionary.Words.CONTENT_URI,
                    UserDictionary.Words.WORD
            );
            sources.put(MainActivity.DICTIONNARY_ACTIVITY, dictionnaryData);

            this.sources = sources;
        }

        return sources.get(getIntent().getStringExtra(MainActivity.MODE));
    }

    /**
     * Remove given data from source list (contacts or dictionnary) and refresh list view
     */
    private void removeFromList() {
        ListView lv = findViewById(R.id.words);
        SparseBooleanArray checked = lv.getCheckedItemPositions();

        // checking all list
        for (int i = 0; i < lv.getAdapter().getCount(); i++) {
            // if checked
            if (checked.get(i)) {
                // add to array list
                getContentResolver().delete(getSource().getUri(), getSource().getColumn() + " = ?", new String[]{this.sourceList.get(i)});
            }
        }
        //remove selected
        lv.clearChoices();
        //update listviewer
        this.arrayAdapter.notifyDataSetChanged();
        //repopulate list view
        this.populateListView();
    }

    /**
     * @return Sort order based on switch position
     */
    private String getSortOrder() {
        //is the button switched or not
        boolean isSwitched = ((Switch) findViewById(R.id.sortOrder)).isChecked();

        return isSwitched ? "ASC" : "DESC";
    }

    /**
     *
     */
    private void populateListView() {
        ListView listView = findViewById(R.id.words);

        // Instanciating an array list
        this.sourceList = getSourceList();
        // This is the array adapter
        this.arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_checked,
                this.sourceList
        );

        listView.setAdapter(this.arrayAdapter);
    }

    protected ArrayList<String> getSourceList() {
        ArrayList<String> entries = new ArrayList<>();
        String[] columns = {getSource().getColumn()};
        String condition = getSource().getColumn() + " LIKE ? ";
        // ? in condition will be replaced by `args` in order.
        String[] args = {"%%"};

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(getSource().getUri(), columns, condition, args, getSource().getColumn() + " " + getSortOrder());
        System.out.println(cursor);
        if (cursor != null) {
            int index = cursor.getColumnIndex(getSource().getColumn());
            //iterate over all words found
            while (cursor.moveToNext()) {
                //gets the value from the column.
                entries.add(cursor.getString(index));
            }
            //close cursor
            cursor.close();
        }

        return entries;
    }

    /**
     * @param requestCode given request code when the activity is launched to know who answer us
     * @param resultCode  the result code
     * @param data        the data given by the intent
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // when there is a result for one activity, handle it
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MainActivity.ADD_WORD_DICTIONNARY_ACTIVITY) {//if from add dictionnary, do something
                String word = data.getStringExtra("word");
                if (!this.sourceList.contains(word)) {
                    UserDictionary.Words.addWord(getApplicationContext(), word, 1, null, Locale.ENGLISH);
                }else{
                    Toast.makeText(this, "Word is already added to the dictionnary", Toast.LENGTH_SHORT).show();
                }
            } else {//otherwise, we can't proceed the query
                return;
            }
        }

        //refresh listview
        populateListView();
    }
}